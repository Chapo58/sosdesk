<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'secure'], function () {
    Route::get('update', 'UpdateController@update');

    //REPORTS
    Route::get('reports/envato/earnings', 'ReportsController@envatoEarnings');
    Route::get('reports/tickets/count/daily', 'ReportsController@dailyTicketCount');
    Route::get('reports/tickets/range', 'ReportsController@generateTicketsReport');

    //USER TAGS
    Route::post('users/{id}/tags/sync', 'UserTagsController@sync');

    //USER DETAILS
    Route::put('users/{id}/details', 'UserDetailsController@update');

    //USER EMAILS
    Route::post('users/{id}/emails/attach', 'UserEmailsController@attach');
    Route::post('users/{id}/emails/detach', 'UserEmailsController@detach');

    //USER TICKETS
    Route::get('users/{userId}/tickets', 'UserTicketsController@index');

    //TICKETS
    Route::get('tickets', 'TicketController@index');
    Route::post('tickets', 'TicketController@store');
    Route::post('tickets/merge/{ticket1}/{ticket2}', 'TicketsMergeController@merge');
    Route::get('tickets/{id}', 'TicketController@show');
    Route::post('tickets/assign', 'TicketAssigneeController@change');
    Route::delete('tickets', 'TicketController@destroy');
    Route::get('tickets/{id}/replies', 'TicketRepliesController@index');
    Route::post('tickets/{id}/{type}', 'TicketRepliesController@store')->where('type', 'drafts|replies|notes');

    //AGENT SEARCH
    Route::get('search/all/{query}', 'SearchController@all');
    Route::get('search/users/{query}', 'SearchController@users');
    Route::get('search/tickets/{query}', 'SearchController@tickets');
    Route::get('search/articles/{query}', 'SearchController@articles');

    //REPLIES
    Route::get('replies/{id}', 'RepliesController@show');
    Route::get('replies/{id}/original', 'OriginalReplyEmailController@show');
    Route::put('replies/{id}', 'RepliesController@update');
    Route::delete('replies/{id}', 'RepliesController@destroy');

    //DRAFTS
    Route::delete('drafts/{id}', 'DraftsController@destroy');
    Route::post('drafts/{draftId}/uploads/{uploadId}/detach', 'DraftUploadsController@detach');

    //TICKET TAGS
    Route::post('tickets/status/change', 'TicketStatusController@change');
    Route::post('tickets/tags/add', 'TicketTagsController@add');
    Route::post('tickets/tags/remove', 'TicketTagsController@remove');

    //TAGS
    Route::get('tags/agent-mailbox', 'TagController@tagsForAgentMailbox');
    Route::get('tags', 'TagController@index');
    Route::post('tags', 'TagController@store');
    Route::put('tags/{id}', 'TagController@update');
    Route::delete('tags/delete-multiple', 'TagController@deleteMultiple');

    //NEW TICKET CATEGORIES
    Route::get('new-ticket/categories', 'NewTicketCategoriesController@index');

    //CANNED REPLIES
    Route::get('canned-replies', 'CannedRepliesController@index');
    Route::post('canned-replies', 'CannedRepliesController@store');
    Route::put('canned-replies/{id}', 'CannedRepliesController@update');
    Route::delete('canned-replies', 'CannedRepliesController@destroy');

    //HELP CENTER
    Route::get('help-center', 'HelpCenterController@index');
    Route::get('help-center/sidenav', 'HelpCenterController@sidenav');

    //HELP CENTER CATEGORIES
    Route::get('help-center/categories', 'CategoriesController@index');
    Route::get('help-center/categories/{id}', 'CategoriesController@show');
    Route::post('help-center/categories', 'CategoriesController@store');
    Route::post('help-center/categories/reorder', 'CategoriesOrderController@change');
    Route::put('help-center/categories/{id}', 'CategoriesController@update');
    Route::post('help-center/categories/{id}/detach-parent', 'ChildCategoryController@detachParent');
    Route::delete('help-center/categories/{id}', 'CategoriesController@destroy');

    //HELP CENTER ARTICLES
    Route::get('help-center/articles/{id}', 'ArticlesController@show');
    Route::get('help-center/articles', 'ArticlesController@index');
    Route::post('help-center/articles', 'ArticlesController@store');
    Route::put('help-center/articles/{id}', 'ArticlesController@update');
    Route::post('help-center/articles/{id}/feedback', 'ArticleFeedbackController@submit');
    Route::post('images/static/upload', 'StaticImagesController@upload');
    Route::delete('help-center/articles', 'ArticlesController@destroy');

    //TRIGGERS
    Route::get('triggers', 'TriggersController@index');
    Route::get('triggers/conditions', 'ConditionsController@index');
    Route::get('triggers/actions', 'ActionsController@index');
    Route::get('triggers/value-options/{name}', 'TriggerValueOptionsController@show');
    Route::get('triggers/{id}', 'TriggersController@show');
    Route::post('triggers', 'TriggersController@store');
    Route::put('triggers/{id}', 'TriggersController@update');
    Route::delete('triggers', 'TriggersController@destroy');

    //ENVATO
    Route::get('envato/validate-purchase-code', 'EnvatoController@validateCode');
    Route::post('envato/items/import', 'EnvatoController@ImportItems');

    //HElP CENTER IMPORT/EXPORT
    Route::post('help-center/actions/import', 'HelpCenterActionsController@import');
    Route::get('help-center/actions/export', 'HelpCenterActionsController@export');
    Route::post('help-center/actions/delete-unused-images', 'HelpCenterActionsController@deleteUnusedImages');

    //3RD PARTY IMPORT/EXPORT
    Route::get('ticketing/actions/helpscout/import', 'TicketingActionsController@importHelpScoutConversations');
});

//TICKETS MAIL WEBHOOKS
Route::post('tickets/mail/incoming', 'TicketsMailController@handleIncoming');
Route::post('tickets/mail/failed', 'TicketsMailController@handleFailed');

//FRONT-END ROUTES THAT NEED TO BE PRE-RENDERED
//Route::get('/', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:home');
Route::get('/', function () {
    return view('index');
});
Route::get('help-center', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:home');
Route::get('help-center/articles/{articleId}/{slug}', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:article');
Route::get('help-center/articles/{parentId}/{articleId}/{slug}', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:article');
Route::get('help-center/articles/{parentId}/{childId}/{articleId}/{slug}', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:article');
Route::get('help-center/categories/{categoryId}/{slug}', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:category');
Route::get('help-center/search/{query}', '\Common\Core\Controllers\HomeController@index')->middleware('prerenderIfCrawler:search');

//CATCH ALL ROUTES AND REDIRECT TO HOME
Route::get('{all}', '\Common\Core\Controllers\HomeController@index')->where('all', '.*');