<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SOSDesk — Centro de Ayuda</title>
    <!-- Meta Share -->
    <meta property="og:title" content="Start.ly — Agency One Page Parallax Template" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="images/screen.jpg" />
    <!-- CSS Files -->
    <link href="https://fonts.googleapis.com/css?family=Product+Sans:300,400,700" rel="stylesheet">
    <!-- build:css css/app.min.css -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('landing/css/global/bootstrap.min.css')}}">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('landing/css/global/plugins/icon-font.css')}}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}">
    <!-- /build -->
</head>

<body class="overflow-hidden">
    <header id="home">

        <!-- navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <h3 class="gradient-mask"><img src="{{url('landing/images/logo.png')}}" alt="SOSDesk" style="max-width:200px;"/></h3>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#site-nav" aria-controls="site-nav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

                <div class="collapse navbar-collapse" id="site-nav">
                    <ul class="navbar-nav text-sm-left ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#features">Caracteristicas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#info">Información</a>
                        </li>
                        <li class="nav-item text-center">
                            <a href="/login" class="btn align-middle btn-outline-primary my-2 my-lg-0">Demo Admin</a>
                        </li>
                        <li class="nav-item text-center">
                            <a href="/help-center" class="btn align-middle btn-primary my-2 my-lg-0">Demo Cliente</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <!-- // end navbar -->


        <!-- hero -->
        <section class="jumbotron-two">

            <div class="container">

                <div class="row align-items-center">
                    <div class="col-12 col-md-5">

                        <h1 class="display-5">Plataforma de ayuda para clientes multiuso</h1>
                        <p class="text-muted mb-3">SOSDesk es un Centro de Ayuda para que sus clientes puedan  evacuar sus dudas sin necesidad de comunicarse con un técnico evitando así consultas repetitivas que consumen tiempo y esfuerzo.</p>
                        <p>
                        	<a href="/login" class="btn btn-xl btn-outline-primary">Demo Admin</a>
                            <a href="/help-center" class="btn btn-xl btn-primary">Demo Cliente</a>
                        </p>

                    </div>
                    <div class="col-12 col-md-7 my-3 my-md-lg">
                        <img src="{{url('landing/images/macbook.png')}}" alt="image" class="img-fluid">
                    </div>
                </div>

            </div>

        </section>
        <!-- // end hero -->


        <div class="bg-shape"></div>
        <div class="bg-circle"></div>
        <div class="bg-circle-two"></div>

    </header>

    <div class="section bg-light pt-lg" id="features">
        <div class="container">

            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-server pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Base de Información</h5> Toda la información que sus clientes necesitan sincronizada en un solo lugar.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-way pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Simple</h5> En cuanto el cliente ingresa al sistema ya tiene a su disposición toda la información, sin pasos adicionales.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-graph1 pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Reportes</h5> Estadísticas completas de lo que los clientes buscan, que información les fue útil y cual no.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-key pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Social Login</h5> Los clientes pueden usar su cuenta de Google+, Facebook o Twiter para ingresar al sistema.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-phone pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Multiplataforma</h5> Funciona en todos los dispositivos ya sean celulares, tablets, notebooks o PCs de escritorio.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media mb-5">
                        <div class="media-icon d-flex mr-3"> <i class="pe-7s-world pe-3x"></i> </div>
                        <!-- // end .di -->
                        <div class="media-body">
                            <h5 class="mt-0">Multilenguaje</h5> El sistema cuenta con herramientas de traducción para que su información esté disponible en cuantos idiomas necesite.
                        </div>
                    </div>
                </div>
                <!-- // end .col -->
            </div>
        </div>
    </div>

    <!-- Features -->
    <div class="section" id="info">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-8">
                    <div class="browser-window limit-height my-5 mr-0 mr-sm-5">
                        <div class="top-bar">
                            <div class="circles">
                                <div class="circle circle-red"></div>
                                <div class="circle circle-yellow"></div>
                                <div class="circle circle-blue"></div>
                            </div>
                        </div>
                        <div class="content">
                            <img src="{{url('landing/images/screen.jpg')}}" alt="image">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="media">
                        <div class="media-body">
                            <div class="media-icon mb-3"> <i class="pe-7s-help1 pe-3x"></i> </div>
                            <h3 class="mt-0">Sistema de Tickets</h3>
                            <p> La plataforma cuenta con la posibilidad de gestionar tickets de sus clientes donde puede darle un seguimiento a cada consulta y asignar técnicos especializados a cada una, diferenciándolas por prioridad, estado y fechas límites.</p>
                        </div>
                    </div>
                </div>

            </div>



            <div class="row align-items-center mt-5">

                <div class="col-sm-4">
                    <div class="media">
                        <div class="media-body">
                            <div class="media-icon mb-3"> <i class="pe-7s-paint pe-3x"></i> </div>
                            <h3 class="mt-0">Personalización Total</h3>
                            <p> El usuario administrador puede modificar la apariencia de cualquier parte del sistema a gusto para asociar la imagen corporativa de la empresa al sistema de ayuda de sus clientes a la vez que puede asignar distintos tipos de roles a cada usuario y limitar los permisos de acceso a los módulos de la plataforma.</p>
                        </div>
                    </div>
                </div>


                <div class="col-sm-8">
                    <img src="{{url('landing/images/screen2.jpg')}}" alt="image" class="img-fluid cast-shadow my-5">
                </div>


            </div>
        </div>



    </div>



    <!-- features -->


    <div class="section bg-light py-lg">
        <div class="container">

            <div class="row">
            	<!--<div class="col-md-6 col-lg-4">
                    <div class="media">
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-medal pe-3x"></i> </div>
                            <h5 class="mt-0">Información Organizada</h5> El sistema está compuesto principalmente por "Artículos" que son agrupados por Categorías y "Etiquetas", asociando así unos artículos con otros y separándolos para que sea más sencillo buscarlos.
                        </div>
                    </div>
                </div>-->
                <div class="col-md-6 col-lg-4">
                    <div class="media">
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-user pe-3x"></i> </div>
                            <h5 class="mt-0">Clientes</h5> Los clientes pueden acceder a la información, calificarla como útil o no, crear Tickets de consulta y gestionar su información personal en el sistema.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media">
                        <!-- // end .di -->
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-pen pe-3x"></i> </div>
                            <h5 class="mt-0">Agentes</h5> Los agentes son los encargados de redactar los artículos, categorizarlos y actualizarlos como así también responder las consultas de los clientes, darles seguimiento y gestionarlas.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="media">
                        <!-- // end .di -->
                        <div class="media-body text-center">
                            <div class="color-icon mb-3"> <i class="pe-7s-id pe-3x"></i> </div>
                            <h5 class="mt-0">Administradores</h5> Los administradores tienen acceso total al sistema, pueden configurarlo, personalizarlo, limitarlo, revisar las estadísticas, dar prioridades, administrar los usuarios y mucho más.
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- // end features -->

    <div class="section bg-light mt-4" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4"> <img src="{{url('landing/images/logo.png')}}" alt="SOSDesk" style="max-width:200px;"/>
                    <p class="mt-3 ml-1 text-muted">Plataforma de ayuda para clientes multiuso. </p>
                </div>
                <div class="col-sm-6">
                    
                </div>
                <div class="col-sm-2">
                    <a href="#home" class="btn btn-sm btn-outline-primary ml-1">Volver al principio</a>
                </div>
            </div>
            <div class=" text-center mt-4"> <small class="text-muted">Copyright ©
                          <script type="text/javascript">
                          document.write(new Date().getFullYear());
                          </script>
                          Todos los derechos reservados. <a href="https://ciatt.com.ar" target="_blank">Ciatt Software</a>
                      </small></div>
        </div>
        <!-- // end .container -->
    </div>
    <!-- // end #about.section -->
    <!-- // end .agency -->
    <!-- JS Files -->
    <!-- build:js js/app.min.js -->
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="{{asset('landing/js/global/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('landing/js/global/bootstrap.bundle.min.js')}}"></script>
    <!-- Main JS -->
    <script src="{{asset('landing/js/script.js')}}"></script>
    <!-- /build -->
</body>

</html>