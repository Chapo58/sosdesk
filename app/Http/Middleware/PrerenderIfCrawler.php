<?php namespace App\Http\Middleware;

use App;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Services\PrerenderUtils;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\HelpCenterController;
use App\Http\Controllers\CategoriesController;
use Common\Core\Middleware\PrerenderIfCrawler as BasePrerender;

class PrerenderIfCrawler extends BasePrerender
{
    /**
     * @param PrerenderUtils $utils
     */
    public function __construct(PrerenderUtils $utils)
    {
        parent::__construct($utils);
    }

    /**
     * @param Request $request
     * @param string $type
     * @return Request|View|null
     */
    public function getResponse($type, Request $request)
    {
        switch ($type) {
            case 'category':
                $category = App::make(CategoriesController::class)->show($request->route('categoryId'))->getData(true)['data'];
                $payload = ['category' => $category, 'utils' => $this->utils];
                return response(view('prerender.category')->with($payload));
            case 'article':
                $article = App::make(ArticlesController::class)->show($request->route('articleId'))->getData(true)['data'];
                $payload = ['article' => $article, 'utils' => $this->utils];
                return response(view('prerender.article')->with($payload));
            case 'search':
                $query = $request->route('query');
                $articles = App::make(SearchController::class)->articles($query)['data'];
                $payload = ['utils' => $this->utils, 'query' => $query, 'articles' => $articles];
                return response(view('prerender.search')->with($payload));
            case 'home':
                $categories = App::make(HelpCenterController::class)->index()->getData(true)['data'];
                $payload = ['utils' => $this->utils, 'categories' => $categories];
                return response(view('prerender.home')->with($payload));
        }

        return null;
    }
}