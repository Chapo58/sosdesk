<?php namespace App\Http\Controllers;

use App\Services\Ticketing\HelpScoutImporter;
use Auth;
use Common\Core\Controller;

class TicketingActionsController extends Controller
{
    /**
     * @var HelpScoutImporter
     */
    private $helpScoutImporter;

    /**
     * TicketingActionsController constructor.
     *
     * @param HelpScoutImporter $helpScoutImporter
     */
    public function __construct(HelpScoutImporter $helpScoutImporter)
    {
        $this->helpScoutImporter = $helpScoutImporter;

        if ( ! Auth::check() || ! Auth::user()->hasPermission('admin')) {
            abort(403);
        }
    }

    /**
     * Import all HelpScout conversations as tickets via their API.
     */
    public function importHelpScoutConversations()
    {
        $this->helpScoutImporter->importConversations();
    }
}
