<?php namespace App\Http\Controllers;

use App\Ticket;
use App\User;
use Common\Core\Controller;
use Illuminate\Database\Eloquent\Collection;

class UserTicketsController extends Controller
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Paginate all users tickets.
     *
     * @param int $userId
     * @return Collection
     */
    public function index($userId)
    {
        $this->authorize('index', [Ticket::class, $userId]);

        $items = $this->user->findOrFail($userId)
            ->tickets()
            ->with('tags', 'latest_reply')
            ->withCount('replies')
            ->paginate(15);

        //remove html tags from replies
        $items->each(function($ticket) {
            if ($ticket->latest_reply) {
                $ticket->latest_reply->body = str_limit(strip_tags($ticket->latest_reply->body), 335);
            }
        });

        return $items;
    }
}
