<?php namespace App\Http\Controllers;

use Cache;
use App\Category;
use Carbon\Carbon;
use Common\Settings\Settings;
use Common\Core\Controller;

class HelpCenterController extends Controller
{
    /**
     * Category model instance.
     *
     * @var Category $category
     */
    private $category;

    /**
     * Settings Service Instance.
     *
     * @var Settings
     */
    private $settings;

    /**
     * HelpCenterController constructor.
     *
     * @param Category $category
     * @param Settings $settings
     */
    public function __construct(Category $category, Settings $settings)
    {
        $this->category = $category;
        $this->settings = $settings;
    }

    /**
     * Return all help center categories, child categories and articles.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Category::class);

        $categoryLimit = $this->settings->get('hc_home.children_per_category');
        $articleLimit = $this->settings->get('hc_home.articles_per_category');
        $data = $this->getHelpCenterData('hc.home', $categoryLimit, $articleLimit);

        return $this->success(['data' => $data]);
    }

    /**
     * Return data for rendering help center sidebar navigation.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function sidenav()
    {
        $this->authorize('index', Category::class);

        $data = $this->getHelpCenterData('hc.sidebar', 6, 25);

        return $this->success(['data' => $data]);
    }

    private function getHelpCenterData($cacheKey, $categoryLimit = 6, $articleLimit = 5)
    {
        return Cache::remember($cacheKey, Carbon::now()->addDays(2), function() use($categoryLimit, $articleLimit) {
            //load categories with children and articles
            $categories = $this->category
                ->rootOnly()
                ->withCount('children')
                ->with(['children' => function($query) { $query->withCount('articles')->with(['articles' => function($query) {
                    $query->orderByPosition()->select('id', 'title', 'position', 'slug');
                }])->orderByPosition(); }])
                ->orderByPosition()->limit(10)->get();

            //limit children and categories
            return $categories->each(function($category) use($categoryLimit, $articleLimit) {

                $category->setRelation('children', $category->children->take($categoryLimit));

                $category->children->each(function($child) use($articleLimit) {
                    $child->setRelation('articles', $child->articles->take($articleLimit));
                });
            });
        });
    }
}
