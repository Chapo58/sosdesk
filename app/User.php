<?php namespace App;

use Illuminate\Database\Eloquent\Builder;
use Laravel\Scout\Searchable;
use App\Traits\FormatsPermissions;
use Illuminate\Notifications\Notifiable;
use Common\Auth\User as CommonUser;

/**
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Ticket[] $tickets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Reply[] $replies
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CannedReply[] $cannedReplies
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PurchaseCode[] $envato_purchase_codes
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $readNotifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PurchaseCode[] $purchase_codes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Email[] $secondary_emails
 * @property-read UserDetails $details
 * @property string $language
 * @property string $country
 * @property string $timezone
 */
class User extends CommonUser
{
    use Notifiable, FormatsPermissions, Searchable;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id'         => $this->id,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'email'      => $this->email,
            'envato_username' => $this->purchase_codes->pluck('envato_username')
        ];
    }

    /**
     * Tickets created by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany('App\Ticket')->orderBy('created_at', 'desc');
    }

    /**
     * User profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function details()
    {
        return $this->hasOne(UserDetails::class);
    }

    /**
     * Secondary email address belonging to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function secondary_emails()
    {
        return $this->hasMany(Email::class);
    }

    /**
     * Envato purchase codes attached to this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchase_codes()
    {
        return $this->hasMany('App\PurchaseCode')->orderBy('created_at', 'desc');
    }

    /**
     * Replies submitted by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    /**
     * Canned replies created by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cannedReplies()
    {
        return $this->hasMany('App\CannedReply');
    }

    /**
     * Tags that are attached to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }

    /**
     * Search users using basic mysql LIKE query.
     *
     * @param string $query
     * @return Builder
     */
    public function basicSearch($query)
    {
        return $this->where('email', 'LIKE', "$query%")
            ->orWhere('first_name', 'LIKE', "$query%")
            ->orWhere('last_name', 'LIKE', "$query%");
    }

    /**
     * Create new envato purchase code from
     * specified details and attach it to user.
     *
     * @param array $purchases
     * @param string $envatoUsername
     */
    public function updatePurchases($purchases, $envatoUsername = null) {
        foreach ($purchases as $purchaseDetails) {
            $this->purchase_codes()->updateOrCreate(['code' => $purchaseDetails['code']], [
                'item_name' => $purchaseDetails['item']['name'],
                'item_id'   => $purchaseDetails['item']['id'],
                'code'      => $purchaseDetails['code'],
                'supported_until' => array_get($purchaseDetails, 'supported_until'),
                'url'       => array_get($purchaseDetails, 'item.url'),
                'image'     => array_get($purchaseDetails, 'item.previews.icon_preview.icon_url'),
                'envato_username' => $envatoUsername,
            ]);
        }
    }

    /**
     * Check if user is a super admin.
     *
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->hasPermission('superAdmin') || $this->hasPermission('admin');
    }

    /**
     * Check if user is an agent.
     *
     * @return bool
     */
    public function isAgent()
    {
        return $this->isSuperAdmin() || $this->belongsToRole('agents');
    }

    /**
     * Check if user belongs to specified group.
     *
     * @param string $name
     * @return bool
     */
    public function belongsToRole($name)
    {
        return $this->roles->contains('name', $name);
    }
}
