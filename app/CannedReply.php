<?php namespace App;

use Common\Files\FileEntry;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $body
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Common\Files\FileEntry[] $uploads
 * @property-read \App\User $user
 * @mixin \Eloquent
 */
class CannedReply extends Model
{
    /**
     * @var array
     */
    protected $casts = ['id' => 'integer', 'user_id' => 'integer'];

    /**
     * @var array
     */
    protected $fillable = ['body', 'name', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function uploads()
    {
        return $this->morphToMany(FileEntry::class, 'model', 'file_entry_models')
            ->orderBy('created_at', 'desc');
    }

    /**
     * Many to one relationship with user model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
