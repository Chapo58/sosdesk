-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-09-2018 a las 07:49:26
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sosdesk`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actions`
--

CREATE TABLE `actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `display_name` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `aborts_cycle` tinyint(1) NOT NULL DEFAULT '0',
  `updates_ticket` tinyint(1) NOT NULL DEFAULT '1',
  `input_config` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actions`
--

INSERT INTO `actions` (`id`, `display_name`, `name`, `aborts_cycle`, `updates_ticket`, `input_config`) VALUES
(1, 'Ticket: Move to Category', 'move_ticket_to_category', 0, 1, '{\"inputs\":[{\"type\":\"select\",\"select_options\":\"category:tags\",\"name\":\"category_name\"}]}'),
(2, 'Notify: via Email', 'send_email_to_user', 0, 0, '{\"inputs\":[{\"type\":\"select\",\"name\":\"agent_id\",\"default_value\":\"(current user)\",\"select_options\":\"agent:id\"},{\"placeholder\":\"Subject\",\"type\":\"text\",\"name\":\"subject\"},{\"placeholder\":\"Email Message\",\"type\":\"textarea\",\"name\":\"message\"}]}'),
(3, 'Ticket: Add a note', 'add_note_to_ticket', 0, 1, '{\"inputs\":[{\"type\":\"textarea\",\"placeholder\":\"Note Text\",\"name\":\"note_text\"}]}'),
(4, 'Ticket: Change status', 'change_ticket_status', 0, 1, '{\"inputs\":[{\"type\":\"select\",\"default_value\":\"open\",\"select_options\":\"ticket:status\",\"name\":\"status_name\"}]}'),
(5, 'Ticket: Assign to Agent', 'assign_ticket_to_agent', 0, 1, '{\"inputs\":[{\"type\":\"select\",\"default_value\":\"(current user)\",\"select_options\":\"agent:id\",\"name\":\"agent_id\"}]}'),
(6, 'Ticket: Add tag(s)', 'add_tags_to_ticket', 0, 1, '{\"inputs\":[{\"type\":\"text\",\"placeholder\":\"Separate tags with comma\",\"name\":\"tags_to_add\"}]}'),
(7, 'Ticket: Remove tag(s)', 'remove_tags_from_ticket', 0, 1, '{\"inputs\":[{\"type\":\"text\",\"placeholder\":\"Separate tags with comma\",\"name\":\"tags_to_remove\"}]}'),
(8, 'Ticket: Delete', 'delete_ticket', 1, 1, '{\"inputs\":[]}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) NOT NULL,
  `body` text NOT NULL,
  `slug` varchar(191) DEFAULT NULL,
  `extra_data` text,
  `draft` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` varchar(191) NOT NULL DEFAULT 'public',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `position` smallint(6) NOT NULL DEFAULT '0',
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `article_feedback`
--

CREATE TABLE `article_feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `was_helpful` tinyint(1) NOT NULL,
  `comment` text,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `billing_plans`
--

CREATE TABLE `billing_plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `currency` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '$',
  `interval` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'month',
  `interval_count` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `uuid` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `recommended` tinyint(1) NOT NULL DEFAULT '0',
  `free` tinyint(1) NOT NULL DEFAULT '0',
  `show_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `features` text COLLATE utf8_unicode_ci,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `available_space` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canned_replies`
--

CREATE TABLE `canned_replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `body` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` text,
  `position` smallint(6) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category_article`
--

CREATE TABLE `category_article` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conditions`
--

CREATE TABLE `conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `type` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `conditions`
--

INSERT INTO `conditions` (`id`, `name`, `type`) VALUES
(1, 'Ticket: Subject', 'ticket:subject'),
(2, 'Ticket: Body', 'ticket:body'),
(3, 'Ticket: Status', 'ticket:status'),
(4, 'Ticket: Is', 'ticket:is'),
(5, 'Ticket: Category', 'ticket:category'),
(6, 'Ticket: Number of Attachments', 'ticket:uploads'),
(7, 'Ticket: Assignee', 'ticket:assignee'),
(8, 'Customer: Name', 'customer:name'),
(9, 'Customer: Email', 'customer:email');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condition_operator`
--

CREATE TABLE `condition_operator` (
  `id` int(10) UNSIGNED NOT NULL,
  `condition_id` int(11) NOT NULL,
  `operator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `condition_operator`
--

INSERT INTO `condition_operator` (`id`, `condition_id`, `operator_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 2, 1),
(9, 2, 2),
(10, 2, 3),
(11, 2, 4),
(12, 3, 10),
(13, 3, 11),
(14, 4, 10),
(15, 4, 11),
(16, 5, 1),
(17, 5, 2),
(18, 5, 3),
(19, 5, 4),
(20, 5, 5),
(21, 5, 6),
(22, 5, 7),
(23, 5, 10),
(24, 5, 11),
(25, 6, 5),
(26, 6, 6),
(27, 6, 8),
(28, 6, 9),
(29, 7, 10),
(30, 7, 11),
(31, 7, 12),
(32, 7, 13),
(33, 7, 14),
(34, 7, 15),
(35, 7, 16),
(36, 7, 17),
(37, 8, 1),
(38, 8, 2),
(39, 8, 3),
(40, 8, 4),
(41, 8, 5),
(42, 8, 6),
(43, 8, 7),
(44, 9, 1),
(45, 9, 2),
(46, 9, 3),
(47, 9, 4),
(48, 9, 5),
(49, 9, 6),
(50, 9, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `address` varchar(191) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `file_entries`
--

CREATE TABLE `file_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `file_name` varchar(191) NOT NULL,
  `file_size` bigint(20) UNSIGNED DEFAULT NULL,
  `mime` varchar(100) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `user_id` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `public_path` varchar(191) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `path` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `file_entry_models`
--

CREATE TABLE `file_entry_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_entry_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localizations`
--

CREATE TABLE `localizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `localizations`
--

INSERT INTO `localizations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'english', '2018-09-24 08:46:38', '2018-09-24 08:46:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mail_templates`
--

CREATE TABLE `mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `display_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `markdown` tinyint(1) NOT NULL DEFAULT '0',
  `action` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mail_templates`
--

INSERT INTO `mail_templates` (`id`, `display_name`, `file_name`, `subject`, `markdown`, `action`, `created_at`, `updated_at`) VALUES
(1, 'Generic', 'generic.blade.php', '{{EMAIL_SUBJECT}}', 0, 'generic', '2018-09-24 08:46:38', '2018-09-24 08:46:38'),
(2, 'Ticket Created Notification', 'ticket-created-notification.blade.php', 'Request received: {{TICKET_SUBJECT}}', 0, 'ticket_created_notification', '2018-09-24 08:46:39', '2018-09-24 08:46:39'),
(3, 'Ticket Rejected Notification', 'ticket-rejected-notification.blade.php', 'Request rejected.', 0, 'ticket_rejected_notification', '2018-09-24 08:46:39', '2018-09-24 08:46:39'),
(4, 'Ticket Reply', 'ticket-reply.blade.php', 'RE: {{TICKET_SUBJECT}}', 0, 'ticket_reply', '2018-09-24 08:46:39', '2018-09-24 08:46:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_04_127_156842_create_social_profiles_table', 1),
(4, '2015_04_127_156842_create_users_oauth_table', 1),
(5, '2015_05_29_131549_create_settings_table', 1),
(6, '2016_03_18_142141_add_email_confirmation_to_users_table', 1),
(7, '2016_05_10_172103_create_tickets_table', 1),
(8, '2016_05_12_190852_create_tags_table', 1),
(9, '2016_05_12_190958_create_taggables_table', 1),
(10, '2016_05_20_181143_create_replies_table', 1),
(11, '2016_05_26_170044_create_uploads_table', 1),
(12, '2016_05_27_143158_create_uploadables_table', 1),
(13, '2016_06_05_164503_create_canned_replies_table', 1),
(14, '2016_06_19_155233_create_articles_table', 1),
(15, '2016_06_19_155255_create_categories_table', 1),
(16, '2016_06_19_171231_create_category_article_table', 1),
(17, '2016_07_14_153703_create_groups_table', 1),
(18, '2016_07_14_153921_create_user_group_table', 1),
(19, '2016_10_14_155303_create_article_feedback_table', 1),
(20, '2016_10_27_130211_create_triggers_table', 1),
(21, '2016_10_27_131019_create_conditions_table', 1),
(22, '2016_10_27_132745_create_operators_table', 1),
(23, '2016_10_27_140007_create_condition_operator_table', 1),
(24, '2016_10_28_142829_create_actions_table', 1),
(25, '2016_10_30_174923_create_trigger_condition_table', 1),
(26, '2016_10_31_162428_create_trigger_action_table', 1),
(27, '2016_12_06_175738_create_purchase_codes_table', 1),
(28, '2017_04_17_113507_create_emails_table', 1),
(29, '2017_04_17_113917_create_profiles_table', 1),
(30, '2017_05_11_132720_rename_profiles_table', 1),
(31, '2017_06_04_122515_create_failed_jobs_table', 1),
(32, '2017_07_02_120142_create_pages_table', 1),
(33, '2017_07_11_122825_create_localizations_table', 1),
(34, '2017_07_17_135837_create_mail_templates_table', 1),
(35, '2017_08_17_144518_create_search_terms_table', 1),
(36, '2017_08_26_131330_add_private_field_to_settings_table', 1),
(37, '2017_09_17_144728_add_columns_to_users_table', 1),
(38, '2017_09_17_152854_make_password_column_nullable', 1),
(39, '2017_09_30_152855_make_settings_value_column_nullable', 1),
(40, '2017_10_01_152897_add_public_column_to_uploads_table', 1),
(41, '2017_12_04_132911_add_avatar_column_to_users_table', 1),
(42, '2018_01_10_140732_create_subscriptions_table', 1),
(43, '2018_01_10_140746_add_billing_to_users_table', 1),
(44, '2018_01_10_161706_create_billing_plans_table', 1),
(45, '2018_03_06_123008_add_envato_username_column', 1),
(46, '2018_06_02_143319_create_user_file_entry_table', 1),
(47, '2018_07_24_113757_add_available_space_to_billing_plans_table', 1),
(48, '2018_07_24_124254_add_available_space_to_users_table', 1),
(49, '2018_07_26_142339_rename_groups_to_roles', 1),
(50, '2018_07_26_142842_rename_user_role_table_columns_to_roles', 1),
(51, '2018_08_07_124200_rename_uploads_to_file_entries', 1),
(52, '2018_08_07_124327_refactor_file_entries_columns', 1),
(53, '2018_08_07_130653_add_folder_path_column_to_file_entries_table', 1),
(54, '2018_08_07_140440_migrate_file_entry_users_to_many_to_many', 1),
(55, '2018_08_15_132225_move_uploads_into_subfolders', 1),
(56, '2018_08_31_104145_rename_uploadables_table', 1),
(57, '2018_08_31_104325_rename_file_entry_models_table_columns', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operators`
--

CREATE TABLE `operators` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `display_name` varchar(191) NOT NULL,
  `type` varchar(191) NOT NULL DEFAULT 'primitive',
  `value_type` varchar(191) NOT NULL DEFAULT 'text',
  `value_placeholder` varchar(191) DEFAULT NULL,
  `validation_rules` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `operators`
--

INSERT INTO `operators` (`id`, `name`, `display_name`, `type`, `value_type`, `value_placeholder`, `validation_rules`) VALUES
(1, 'contains', 'Contains', 'primitive', 'text', NULL, NULL),
(2, 'not_contains', 'Does not contain', 'primitive', 'text', NULL, NULL),
(3, 'starts_with', 'Starts with', 'primitive', 'text', NULL, NULL),
(4, 'ends_with', 'Ends with', 'primitive', 'text', NULL, NULL),
(5, 'equals', 'Equals', 'primitive', 'text', NULL, NULL),
(6, 'not_equals', 'Does not equal', 'primitive', 'text', NULL, NULL),
(7, 'matches_regex', 'Matches regex pattern', 'primitive', 'text', NULL, NULL),
(8, 'more', 'More then', 'primitive', 'text', NULL, NULL),
(9, 'less', 'Less then', 'primitive', 'text', NULL, NULL),
(10, 'is', 'Is', 'primitive', 'text', NULL, NULL),
(11, 'not', 'Is not', 'primitive', 'text', NULL, NULL),
(12, 'changed', 'Changed', 'mixed', 'text', NULL, NULL),
(13, 'changed_to', 'Changed to', 'mixed', 'text', NULL, NULL),
(14, 'changed_from', 'Changed from', 'mixed', 'text', NULL, NULL),
(15, 'not_changed', 'Not changed', 'mixed', 'text', NULL, NULL),
(16, 'not_changed_to', 'Not changed to', 'mixed', 'text', NULL, NULL),
(17, 'not_changed_from', 'Not changed from', 'mixed', 'text', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchase_codes`
--

CREATE TABLE `purchase_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) NOT NULL,
  `user_id` varchar(191) NOT NULL,
  `item_name` varchar(191) NOT NULL,
  `item_id` varchar(191) NOT NULL,
  `supported_until` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `image` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `envato_username` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `replies`
--

CREATE TABLE `replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `type` varchar(191) NOT NULL DEFAULT 'replies',
  `uuid` varchar(30) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `default` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `guests` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `default`, `guests`, `created_at`, `updated_at`) VALUES
(1, 'customers', '{\"categories.view\":1,\"articles.view\":1,\"tags.view\":1,\"tickets.create\":1,\"files.create\":1,\"pages.view\":1}', 1, 0, '2018-09-24 08:46:38', '2018-09-24 08:46:38'),
(2, 'agents', '{\"categories.view\":1,\"articles.view\":1,\"tags.view\":1,\"tickets.create\":1,\"files.create\":1,\"pages.view\":1,\"files.view\":1,\"tickets.view\":1,\"tickets.update\":1,\"tickets.delete\":1,\"replies.view\":1,\"replies.create\":1,\"replies.update\":1,\"replies.delete\":1,\"users.view\":1,\"access.admin\":1,\"canned_replies.view\":1,\"canned_replies.create\":1,\"actions.view\":1,\"conditions.view\":1,\"triggers.view\":1,\"triggers.create\":1}', 0, 0, '2018-09-24 08:46:38', '2018-09-24 08:46:38'),
(3, 'guests', '{\"users.view\":1,\"pages.view\":[1,1],\"categories.view\":1,\"articles.view\":1,\"tags.view\":1}', 0, 1, '2018-09-24 08:46:39', '2018-09-24 08:46:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `search_terms`
--

CREATE TABLE `search_terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `term` varchar(191) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `type` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`, `private`) VALUES
(1, 'dates.format', 'yyyy-MM-dd', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(2, 'dates.locale', 'en_US', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(3, 'social.google.enable', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(4, 'social.twitter.enable', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(5, 'social.facebook.enable', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(6, 'registration.disable', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(7, 'branding.use_custom_theme', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(8, 'branding.favicon', 'favicon.ico', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(9, 'branding.logo_dark', 'client/assets/images/logo-dark.png', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(10, 'branding.logo_light', 'client/assets/images/logo-light.png', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(11, 'i18n.default_localization', 'english', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(12, 'i18n.enable', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(13, 'logging.sentry_public', NULL, '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(14, 'homepage.type', 'default', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(15, 'billing.enable', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(16, 'billing.paypal_test_mode', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(17, 'billing.stripe_test_mode', '1', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(18, 'billing.stripe.enable', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(19, 'billing.paypal.enable', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(20, 'billing.accepted_cards', '[\"visa\",\"mastercard\",\"american-express\",\"discover\"]', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(21, 'branding.site_name', 'BeDesk', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(22, 'cache.report_minutes', '60', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(23, 'site.force_https', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(24, 'menus', '[{\"name\":\"Agent Mailbox\",\"position\":\"agent-mailbox\",\"items\":[{\"type\":\"route\",\"order\":1,\"label\":\"Tickets\",\"action\":\"mailbox\"},{\"type\":\"route\",\"order\":2,\"label\":\"Help Center\",\"action\":\"help-center\\/manage\"},{\"type\":\"route\",\"order\":2,\"label\":\"Admin Area\",\"action\":\"admin\"}]},{\"name\":\"Header Menu\",\"position\":\"header\",\"items\":[{\"type\":\"route\",\"condition\":\"auth\",\"label\":\"My Tickets\",\"action\":\"\\/help-center\\/tickets\"}]}]', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(25, 'uploads.max_size', '52428800', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(26, 'uploads.available_space', '104857600', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(27, 'uploads.blocked_extensions', '[\"exe\",\"application\\/x-msdownload\",\"x-dosexec\"]', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(28, 'replies.send_email', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(29, 'tickets.create_from_emails', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(30, 'tickets.send_ticket_created_notification', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(31, 'replies.default_redirect', 'next_active_ticket', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(32, 'mail.handler', 'null', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 1),
(33, 'mail.webhook_secret_key', 'YKBVgRkdBK47CxfuazITjUZNb9JLty', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 1),
(34, 'mail.use_default_templates', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(35, 'mail.store_unmatched_emails', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(36, 'realtime.enable', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(37, 'realtime.pusher_key', NULL, '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(38, 'envato.filter_search', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(39, 'articles.default_order', 'created_at|desc', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(40, 'hc.search_page_limit', '20', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(41, 'hc.search_page.body_limit', '300', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(42, 'hc_home.children_per_category', '6', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(43, 'hc_home.articles_per_category', '5', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(44, 'hc_home.hide_small_categories', '0', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(45, 'hc.home.title', 'How can we help you?', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(46, 'hc.home.subtitle', 'Ask Questions. Browse Articles. Find Answers.', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(47, 'hc.home.search-placeholder', 'Enter your question or keyword here', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(48, 'hc.home.background', 'assets/images/pattern.svg', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(49, 'hc.new-ticket.title', 'Submit a Ticket', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(50, 'hc.new-ticket.category_label', 'Select the item you need help with', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(51, 'hc.new-ticket.subject_label', 'In a few words, tell us what your enquiry is about', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(52, 'hc.new-ticket.description_label', 'Tell us more...Please be as detailed as possible.', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(53, 'hc.new-ticket.submit_button_text', 'Submit', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(54, 'hc.new-ticket.sidebar_title', 'Before you submit:', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(55, 'hc.new-ticket.sidebar_tips', '[{\"title\":\"Tell us!\",\"content\":\"Add as much detail as possible, including site and page name.\"},{\"title\":\"Show us!\",\"content\":\"Add a screenshot or a link to a video.\"}]', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(56, 'seo.article_title', '{{ARTICLE_TITLE}}', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(57, 'seo.article_description', '{{ARTICLE_DESCRIPTION}}', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(58, 'seo.category_title', '{{CATEGORY_NAME}}', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(59, 'seo.category_description', '{{CATEGORY_DESCRIPTION}}', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(60, 'seo.search_title', 'Search results for {{QUERY}}', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(61, 'seo.search_description', 'Search results for {{QUERY}}', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(62, 'seo.homepage_title', 'Help Center', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0),
(63, 'seo.homepage_description', 'Articles, Tutorials, FAQ\'s and more.', '2018-09-24 08:46:39', '2018-09-24 08:46:39', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_profiles`
--

CREATE TABLE `social_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_service_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `plan_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `gateway` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `gateway_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `quantity` int(11) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8_unicode_ci,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `renews_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taggables`
--

CREATE TABLE `taggables` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'custom',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `name`, `display_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'open', NULL, 'status', '2018-09-24 08:46:36', '2018-09-24 08:46:36'),
(2, 'closed', NULL, 'status', '2018-09-24 08:46:36', '2018-09-24 08:46:36'),
(3, 'pending', NULL, 'status', '2018-09-24 08:46:36', '2018-09-24 08:46:36'),
(4, 'spam', NULL, 'status', '2018-09-24 08:46:36', '2018-09-24 08:46:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) NOT NULL,
  `user_id` int(11) NOT NULL,
  `closed_by` int(11) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `closed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `triggers`
--

CREATE TABLE `triggers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` text,
  `times_fired` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trigger_action`
--

CREATE TABLE `trigger_action` (
  `id` int(10) UNSIGNED NOT NULL,
  `trigger_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `action_value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trigger_condition`
--

CREATE TABLE `trigger_condition` (
  `id` int(10) UNSIGNED NOT NULL,
  `trigger_id` int(11) NOT NULL,
  `condition_id` int(11) NOT NULL,
  `operator_id` int(11) NOT NULL,
  `condition_value` text NOT NULL,
  `match_type` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `available_space` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `avatar_url`, `gender`, `permissions`, `email`, `password`, `card_brand`, `card_last_four`, `remember_token`, `created_at`, `updated_at`, `confirmed`, `confirmation_code`, `language`, `country`, `timezone`, `avatar`, `stripe_id`, `available_space`) VALUES
(1, 'Ciatt', NULL, NULL, NULL, NULL, '{\"admin\":1,\"superAdmin\":1}', 'demo@ciatt.com.ar', '$2y$10$phRgjKRq3IX3xfalvjun0eVyWd.VHfLgVyWHsDnYWnDtNJjc0gvVS', NULL, NULL, 'AQkoauiEXnUbFGAbg0eGb6nECIkTS7wxe8FchgLRtpbBrKLU8TbeQMIXQ5wd', '2018-09-24 08:47:13', '2018-09-24 08:47:13', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_oauth`
--

CREATE TABLE `users_oauth` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `service` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `details` text,
  `notes` text,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_file_entry`
--

CREATE TABLE `user_file_entry` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `file_entry_id` int(10) UNSIGNED NOT NULL,
  `owner` tinyint(1) NOT NULL DEFAULT '0',
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_role`
--

CREATE TABLE `user_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actions_name_index` (`name`);

--
-- Indices de la tabla `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_title_index` (`title`),
  ADD KEY `articles_slug_index` (`slug`),
  ADD KEY `articles_draft_index` (`draft`),
  ADD KEY `articles_visibility_index` (`visibility`),
  ADD KEY `articles_views_index` (`views`),
  ADD KEY `articles_position_index` (`position`);

--
-- Indices de la tabla `article_feedback`
--
ALTER TABLE `article_feedback`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_feedback_article_id_user_id_unique` (`article_id`,`user_id`),
  ADD UNIQUE KEY `article_feedback_article_id_ip_unique` (`article_id`,`ip`),
  ADD KEY `article_feedback_article_id_index` (`article_id`),
  ADD KEY `article_feedback_user_id_index` (`user_id`),
  ADD KEY `article_feedback_ip_index` (`ip`);

--
-- Indices de la tabla `billing_plans`
--
ALTER TABLE `billing_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `canned_replies`
--
ALTER TABLE `canned_replies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `canned_replies_name_user_id_unique` (`name`,`user_id`),
  ADD KEY `canned_replies_name_index` (`name`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_name_index` (`name`),
  ADD KEY `categories_position_index` (`position`);

--
-- Indices de la tabla `category_article`
--
ALTER TABLE `category_article`
  ADD UNIQUE KEY `category_article_article_id_category_id_unique` (`article_id`,`category_id`);

--
-- Indices de la tabla `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `conditions_type_index` (`type`);

--
-- Indices de la tabla `condition_operator`
--
ALTER TABLE `condition_operator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `condition_operator_condition_id_operator_id_unique` (`condition_id`,`operator_id`);

--
-- Indices de la tabla `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emails_address_index` (`address`),
  ADD KEY `emails_user_id_index` (`user_id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `file_entries`
--
ALTER TABLE `file_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uploads_file_name_unique` (`file_name`),
  ADD KEY `uploads_name_index` (`name`),
  ADD KEY `uploads_user_id_index` (`user_id`),
  ADD KEY `uploads_public_index` (`public`),
  ADD KEY `file_entries_updated_at_index` (`updated_at`),
  ADD KEY `file_entries_parent_id_index` (`parent_id`),
  ADD KEY `file_entries_type_index` (`type`),
  ADD KEY `file_entries_deleted_at_index` (`deleted_at`),
  ADD KEY `file_entries_path_index` (`path`);

--
-- Indices de la tabla `file_entry_models`
--
ALTER TABLE `file_entry_models`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uploadables_upload_id_uploadable_id_uploadable_type_unique` (`file_entry_id`,`model_id`,`model_type`);

--
-- Indices de la tabla `localizations`
--
ALTER TABLE `localizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `localizations_name_index` (`name`);

--
-- Indices de la tabla `mail_templates`
--
ALTER TABLE `mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail_templates_file_name_unique` (`file_name`),
  ADD UNIQUE KEY `mail_templates_action_unique` (`action`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operators_name_index` (`name`);

--
-- Indices de la tabla `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `purchase_codes`
--
ALTER TABLE `purchase_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_codes_user_id_index` (`user_id`),
  ADD KEY `purchase_codes_item_name_index` (`item_name`),
  ADD KEY `purchase_codes_item_id_index` (`item_id`);

--
-- Indices de la tabla `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `replies_user_id_index` (`user_id`),
  ADD KEY `replies_ticket_id_index` (`ticket_id`),
  ADD KEY `replies_type_index` (`type`),
  ADD KEY `replies_uuid_index` (`uuid`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`),
  ADD KEY `groups_default_index` (`default`),
  ADD KEY `groups_guests_index` (`guests`);

--
-- Indices de la tabla `search_terms`
--
ALTER TABLE `search_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `search_terms_term_index` (`term`),
  ADD KEY `search_terms_user_id_index` (`user_id`),
  ADD KEY `search_terms_count_index` (`count`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_name_unique` (`name`),
  ADD KEY `settings_private_index` (`private`);

--
-- Indices de la tabla `social_profiles`
--
ALTER TABLE `social_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_profiles_user_id_service_name_unique` (`user_id`,`service_name`),
  ADD UNIQUE KEY `social_profiles_service_name_user_service_id_unique` (`service_name`,`user_service_id`),
  ADD KEY `social_profiles_user_id_index` (`user_id`);

--
-- Indices de la tabla `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `taggables`
--
ALTER TABLE `taggables`
  ADD UNIQUE KEY `taggables_tag_id_taggable_id_user_id_taggable_type_unique` (`tag_id`,`taggable_id`,`user_id`,`taggable_type`),
  ADD KEY `taggables_tag_id_index` (`tag_id`),
  ADD KEY `taggables_taggable_id_index` (`taggable_id`),
  ADD KEY `taggables_taggable_type_index` (`taggable_type`),
  ADD KEY `taggables_user_id_index` (`user_id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_name_unique` (`name`),
  ADD KEY `tags_type_index` (`type`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tickets_subject_index` (`subject`),
  ADD KEY `tickets_user_id_index` (`user_id`),
  ADD KEY `tickets_closed_by_index` (`closed_by`),
  ADD KEY `tickets_assigned_to_index` (`assigned_to`),
  ADD KEY `tickets_created_at_index` (`created_at`),
  ADD KEY `tickets_updated_at_index` (`updated_at`),
  ADD KEY `tickets_closed_at_index` (`closed_at`);

--
-- Indices de la tabla `triggers`
--
ALTER TABLE `triggers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trigger_action`
--
ALTER TABLE `trigger_action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trigger_action_trigger_id_index` (`trigger_id`),
  ADD KEY `trigger_action_action_id_index` (`action_id`);

--
-- Indices de la tabla `trigger_condition`
--
ALTER TABLE `trigger_condition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trigger_condition_trigger_id_index` (`trigger_id`),
  ADD KEY `trigger_condition_condition_id_index` (`condition_id`),
  ADD KEY `trigger_condition_operator_id_index` (`operator_id`),
  ADD KEY `trigger_condition_match_type_index` (`match_type`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `users_oauth`
--
ALTER TABLE `users_oauth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_oauth_user_id_service_unique` (`user_id`,`service`),
  ADD UNIQUE KEY `users_oauth_token_unique` (`token`),
  ADD KEY `users_oauth_user_id_index` (`user_id`);

--
-- Indices de la tabla `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_index` (`user_id`);

--
-- Indices de la tabla `user_file_entry`
--
ALTER TABLE `user_file_entry`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_file_entry_file_entry_id_user_id_unique` (`file_entry_id`,`user_id`),
  ADD KEY `user_file_entry_user_id_index` (`user_id`),
  ADD KEY `user_file_entry_file_entry_id_index` (`file_entry_id`),
  ADD KEY `user_file_entry_owner_index` (`owner`);

--
-- Indices de la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_group_user_id_group_id_unique` (`user_id`,`role_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `article_feedback`
--
ALTER TABLE `article_feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `billing_plans`
--
ALTER TABLE `billing_plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `canned_replies`
--
ALTER TABLE `canned_replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `conditions`
--
ALTER TABLE `conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `condition_operator`
--
ALTER TABLE `condition_operator`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `file_entries`
--
ALTER TABLE `file_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `file_entry_models`
--
ALTER TABLE `file_entry_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `localizations`
--
ALTER TABLE `localizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `mail_templates`
--
ALTER TABLE `mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `operators`
--
ALTER TABLE `operators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `purchase_codes`
--
ALTER TABLE `purchase_codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `search_terms`
--
ALTER TABLE `search_terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `social_profiles`
--
ALTER TABLE `social_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `triggers`
--
ALTER TABLE `triggers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trigger_action`
--
ALTER TABLE `trigger_action`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trigger_condition`
--
ALTER TABLE `trigger_condition`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users_oauth`
--
ALTER TABLE `users_oauth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_file_entry`
--
ALTER TABLE `user_file_entry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
