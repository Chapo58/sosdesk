<?php namespace Common\Files\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Common\Core\Controller;
use Common\Files\FileEntry;
use ZipArchive;

class DownloadFileController extends Controller
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var FileEntry
     */
    private $fileEntry;

    /**
     * DownloadFileController constructor.
     * @param Request $request
     * @param FileEntry $fileEntry
     */
    public function __construct(Request $request, FileEntry $fileEntry)
    {
        $this->request = $request;
        $this->fileEntry = $fileEntry;
    }

    public function download()
    {
        // TODO: limit to N records or chunk

        $hashes = explode(',', $this->request->get('hashes'));
        $ids = array_map(function($hash) {
            return $this->fileEntry->decodeHash($hash);
        }, $hashes);

        $entries = $this->fileEntry->whereIn('id', $ids)->get();

        // TODO: refactor file entry policy to accent multiple IDs
        $entries->each(function($entry) {
            $this->authorize('show', [FileEntry::class, $entry]);
        });

        if ($entries->count() === 1 && $entries->first()->type !== 'folder') {
            $entry = $entries->first();

            $drive = \Storage::disk(config('common.site.uploads_disk'));
            $stream = $drive->readStream($entry->getStoragePath());

            return response()->stream(function() use($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Type" => $entry->mime,
                "Content-Length" => $drive->size($entry->getStoragePath()),
                "Content-disposition" => "attachment; filename=\"" . $entry->name . "\"",
            ]);
        } else {
            $path = $this->createZip($entries);
            $timestamp = Carbon::now()->getTimestamp();
            return response()->download($path, "download-$timestamp.zip");
        }
    }

    /**
     * Create a zip archive for download.
     *
     * @param Collection $entries
     * @return string
     */
    private function createZip(Collection $entries) {
        $random = str_random();
        $path = storage_path("app/temp/zips/$random.zip");
        $zip = new ZipArchive();

        $zip->open($path, ZIPARCHIVE::CREATE);

        $this->fillZip($zip, $entries);

        $zip->close();

        return $path;
    }

    /**
     * @param ZipArchive $zip
     * @param Collection $entries
     */
    private function fillZip(ZipArchive $zip, Collection $entries) {
        $childEntries = $entries->filter(function(FileEntry $entry) {
            return $entry->type === 'folder';
        })->map(function(FileEntry $entry) {
            return $entry->findChildren();
        })->collapse();

        // load all child entries
        $merged = $entries->merge($childEntries);

        // all specified entries will be child of same parent folder so we need
        // to load parent once, in order to properly generate paths in zip file
        if ($parent = $entries->first()->parent) {
            $merged->push($parent);
        }

        $merged->each(function(FileEntry $entry) use($zip, $merged) {
            if ($entry->type !== 'folder') {
                $zip->addFromString($this->transformPath($entry, $merged), $this->getFileContents($entry));
            }
        });
    }

    /**
     * Replace entry IDs with names inside "path" property.
     *
     * @param FileEntry $entry
     * @param Collection $folders
     * @return string
     */
    private function transformPath(FileEntry $entry, Collection $folders)
    {
        if ( ! $entry->path) return $entry->getNameWithExtension();

        // folder/file.png => ['folder', 'file.png']
        $path = explode('/', $entry->path);

        // last value will be name of the file itself, need to remove it
        array_pop($path);

        $path = array_map(function($id) use($folders) {
            return $folders->find((int) $id)->name;
        }, $path);

        return implode('/', $path) . '/' . $entry->getNameWithExtension();
    }

    private function getFileContents(FileEntry $entry) {
        return \Storage::disk(config('common.site.uploads_disk'))->get($entry->getStoragePath());
    }


    /**
     * Verify that given password matches the one on file.
     *
     * return Response
     */
    public function checkPassword($file)
    {
        return Hash::check(Input::get('password'), $file->password) || Input::get('password') === $file->password;
    }
}
