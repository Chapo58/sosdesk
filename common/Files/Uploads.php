<?php namespace Common\Files;

use Illuminate\Support\Arr;
use Illuminate\Http\UploadedFile;

class Uploads {

    /**
     * @var FileEntry
     */
    private $fileEntry;

    /**
     * @var FileStorage
     */
    private $storage;

    /**
     * @var FileEntries
     */
    private $fileEntries;

    /**
     * UploadsApiService constructor.
     *
     * @param FileEntry $upload
     * @param FileStorage $storage
     * @param FileEntries $fileEntries
     */
    public function __construct(FileEntry $upload, FileStorage $storage, FileEntries $fileEntries)
    {
        $this->fileEntry = $upload;
        $this->storage = $storage;
        $this->fileEntries = $fileEntries;
    }

    /**
     * Create public uploads from specified file data.
     *
     * @param UploadedFile $file
     * @param array $config
     * @return FileEntry
     */
    public function storePublic($file, $config)
    {
        $file = $this->saveFileToDisk($file, $config);

        return $this->fileEntries->create($file, $config);
    }

    /**
     * Create upload from specified file data.
     *
     * This will upload file to disk (if needed)
     * and create file entry in database for it.
     *
     * @param UploadedFile|array $file
     * @param array $params
     * @return FileEntry
     */
    public function store($file, $params = [])
    {
        $fileEntry = $this->fileEntries->create($file, $params);

        $this->saveFileToDisk($file, $params);

        return $fileEntry;
    }

    /**
     * Save specified files to disk.
     *
     * @param UploadedFile $file
     *
     * @param array $config
     * @return array
     */
    private function saveFileToDisk($file, $config = [])
    {
        //convert UploadedFile instances to array
        if (is_a($file, UploadedFile::class)) {
            $file = $this->uploadedFileToArray($file);
        }

        //store file to disk and set file name on file configuration
        if (Arr::get($config, 'disk') === 'public') {
            $file['file_name'] = $this->saveFileToPublicDisk($file, $config);
        } else {
            $file['file_name'] = $this->storage->put($file['contents'], null, null, $config);
        }

        //free memory
        unset($file['contents']);

        return $file;
    }

    /**
     * Store specified file on public disk.
     *
     * @param array $file
     * @param array $config
     * @return string
     */
    private function saveFileToPublicDisk($file, $config)
    {
        return $this->storage->put(
            $file['contents'],
            $config['publicPath'],
            str_random(40).'.'.$file['extension'],
            ['visibility' => 'public', 'disk' => 'public']
        );
    }
}